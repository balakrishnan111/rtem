﻿using InTheHand.Net.Sockets;
using NAudio.CoreAudioApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace RTEM.Network
{
    class Program
    {
        static void Main(string[] args)
        {
            Utils utils = new Utils();
            Console.WriteLine("------------------------------------------------------");
            Console.WriteLine("HostName : " + Utils.GetHostName());
            Console.WriteLine("HostIP   : " + Utils.GetLocalIPAddress());


            HashSet<string> physicalMacAddresses = new HashSet<string>(Utils.GetMacAddressesOnPciBus());

            foreach (var v in physicalMacAddresses)
                Console.WriteLine("MAC   : " + v);
            Console.WriteLine("------------------------------------------------------");

            foreach (var v in Utils.GetNwProps())
            {
                Console.WriteLine( v.Key + " : " + v.Value);
            }

            Console.WriteLine("------------------------------------------------------");
            var enumerator = new MMDeviceEnumerator();
            foreach (var endpoint in
                     enumerator.EnumerateAudioEndPoints(DataFlow.Render, DeviceState.Active))
            {
                if(endpoint.FriendlyName.Contains("Speaker"))

                    Console.WriteLine("Audio Device : " + endpoint.FriendlyName);
            }
            Console.WriteLine("------------------------------------------------------");

            utils.BuletoothDevices();

            Console.WriteLine("------------------------------------------------------");

            utils.GetWifiSSID();

            Console.WriteLine("------------------------------------------------------");

            Console.ReadLine();

        }
    }
}
