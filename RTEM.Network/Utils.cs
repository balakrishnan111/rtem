﻿using InTheHand.Net;
using InTheHand.Net.Bluetooth;
using InTheHand.Net.Sockets;
using NativeWifi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace RTEM.Network
{
    public class Utils
    {
        public static string GetHostName()
        {
            return Environment.MachineName;
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        public static Dictionary<String, String> GetNwProps()
        {
            Dictionary<String, String> props = new Dictionary<string, string>();
            props["networktype"] = "";


            if (NetworkInterface.GetIsNetworkAvailable())
            {
                props["network"] = "up";
                props["vpn"] = "down";
                NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();
                foreach (NetworkInterface Interface in interfaces)
                {
                    if (Interface.OperationalStatus == OperationalStatus.Up)
                    {
                        // This is the OpenVPN driver for windows. 
                        if (!Interface.Description.Contains("VPN") && !Interface.Description.Contains("Virtual")
                           && !Interface.Description.Contains("Loopback")
                          && Interface.OperationalStatus == OperationalStatus.Up)
                        {
                            var ip = Interface.GetIPProperties().UnicastAddresses.First(x => x.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).Address.ToString();
                            props["physicalip"] = ip;
                            props["physicalname"] = Interface.Description;
                            props["networktype"] = Interface.NetworkInterfaceType.ToString();

                            props["NICType"] = Interface.Name;
                        }

                        // This is the OpenVPN driver for windows. 
                        if (Interface.Description.Contains("VPN")

                          && Interface.OperationalStatus == OperationalStatus.Up)
                        {
                            var ip = Interface.GetIPProperties().UnicastAddresses.First(x => x.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).Address.ToString();

                            props["vpnip"] = ip;
                            props["vpnname"] = Interface.Description;
                            props["vpn"] = "on";

                        }
                    }
                }
            }
            else
            {
                props["network"] = "down";
            }
            return props;
        }

        internal void GetWifiSSID()
        {

            WlanClient client = new WlanClient();
            foreach( var v in  client.Interfaces)
            {
                Console.WriteLine("Current Connected Wifi SSID : " + v.CurrentConnection.profileName);
            }


        }

        internal void BuletoothDevices()
        {
            BluetoothClient client = new BluetoothClient();

            // mac is mac address of local bluetooth device
            BluetoothEndPoint localEndpoint = new BluetoothEndPoint(BluetoothAddress.None, BluetoothService.SerialPort);
            // client is used to manage connections
            BluetoothClient localClient = new BluetoothClient(localEndpoint);
            // component is used to manage device discovery
            BluetoothComponent localComponent = new BluetoothComponent(localClient);

            int i = 0;

            foreach (var v in localClient.DiscoverDevices(255, true, true, false))
            {
                 
                Console.WriteLine("Bluetooth Device " + i + " : Name : " + v.DeviceName);
                Console.WriteLine("Bluetooth Device " + i + " : Connected : " + v.Connected);
                Console.WriteLine("Bluetooth Device " + i + " : Profile : " + v.ClassOfDevice.Service);
                Console.WriteLine("Bluetooth Device " + i + " : Device Type : " + v.ClassOfDevice.Device);
                Console.WriteLine("Bluetooth Device " + i + " : MAC : " + v.DeviceAddress);
                 
            }
        }



        private void component_DiscoverDevicesProgress(object sender, DiscoverDevicesEventArgs e)
        {
            // log and save all found devices
            for (int i = 0; i < e.Devices.Length; i++)
            {
                if (e.Devices[i].Remembered)
                {
                    Console.WriteLine(e.Devices[i].DeviceName + " (" + e.Devices[i].DeviceAddress + "): Device is known");
                }
                else
                {
                    Console.WriteLine(e.Devices[i].DeviceName + " (" + e.Devices[i].DeviceAddress + "): Device is unknown");
                }
                //    this.deviceList.Add(e.Devices[i]);
            }
        }

        private void component_DiscoverDevicesComplete(object sender, DiscoverDevicesEventArgs e)
        {
            // log some stuff
        }

        public static Dictionary<String, String> getaudioprops()
        {
            ManagementObjectSearcher objSearcher = new ManagementObjectSearcher(
         "SELECT * FROM Win32_SoundDevice");

            ManagementObjectCollection objCollection = objSearcher.Get();

            foreach (ManagementObject obj in objCollection)
            {
                foreach (PropertyData property in obj.Properties)
                {

                    Console.Out.WriteLine(String.Format("{0}:{1}", property.Name, property.Value));
                }
            }

            return null;
        }



        public static IEnumerable<string> GetMacAddressesOnPciBus()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher
    ("Select MACAddress,PNPDeviceID FROM Win32_NetworkAdapter WHERE MACAddress IS NOT NULL AND PNPDeviceID IS NOT NULL");
            ManagementObjectCollection mObject = searcher.Get();

            foreach (ManagementObject obj in mObject)
            {
                string pnp = obj["PNPDeviceID"].ToString();
                if (pnp.Contains("PCI\\"))
                {
                    string mac = obj["MACAddress"].ToString();
                    mac = mac.Replace(":", string.Empty);
                    yield return mac;
                }
            }
        }
    }
}
